# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import complaint
from . import purchase
from . import account

__all__ = ['register']


def register():
    Pool.register(
        complaint.Type,
        complaint.Complaint,
        complaint.Action,
        complaint.Action_PurchaseLine,
        complaint.Action_InvoiceLine,
        purchase.Configuration,
        purchase.ConfigurationSequence,
        purchase.Purchase,
        account.InvoiceLine,
        module='purchase_complaint', type_='model')
