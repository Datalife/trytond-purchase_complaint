# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from collections import defaultdict
from decimal import Decimal

from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.model import DeactivableMixin
from trytond.model.exceptions import AccessError
from trytond.pyson import Eval, If, Bool
from trytond.pool import Pool
from trytond.transaction import Transaction

from trytond.modules.product import price_digits


class Type(DeactivableMixin, ModelSQL, ModelView):
    'Supplier Complaint Type'
    __name__ = 'purchase.complaint.type'

    name = fields.Char('Name', required=True)
    origin = fields.Many2One('ir.model', 'Origin', required=True,
        domain=[('model', 'in', ['purchase.purchase', 'purchase.line',
                    'account.invoice', 'account.invoice.line'])])


class Complaint(Workflow, ModelSQL, ModelView):
    "Supplier Complaint"
    __name__ = 'purchase.complaint'
    _rec_name = 'number'

    _states = {
        'readonly': Eval('state') != 'draft',
        }
    _depends = ['state']

    number = fields.Char("Number", readonly=True, select=True)
    reference = fields.Char("Reference", select=True)
    date = fields.Date("Date", states=_states, depends=_depends)
    supplier = fields.Many2One(
        'party.party', "Supplier", required=True, states=_states,
        context={
            'company': Eval('company', -1),
            },
        depends=_depends + ['company'])
    address = fields.Many2One('party.address', "Address",
        domain=[('party', '=', Eval('supplier'))],
        states=_states, depends=_depends + ['supplier'])
    company = fields.Many2One('company.company', "Company", required=True,
        states=_states, depends=_depends)
    employee = fields.Many2One('company.employee', "Employee",
        states=_states, depends=_depends)
    type = fields.Many2One('purchase.complaint.type', "Type", required=True,
        states=_states, depends=_depends)
    origin = fields.Reference("Origin", selection='get_origin',
        states={
            'readonly': ((Eval('state') != 'draft')
                | Bool(Eval('actions', [0]))),
            'required': Bool(Eval('origin_model')),
            },
        depends=['state', 'supplier', 'origin_model', 'company'])
    origin_id = fields.Function(fields.Integer("Origin ID"),
        'on_change_with_origin_id')
    origin_model = fields.Function(fields.Char("Origin Model"),
        'on_change_with_origin_model')
    description = fields.Text("Description", states=_states, depends=_depends)
    actions = fields.One2Many(
        'purchase.complaint.action', 'complaint', "Actions",
        states={
            'readonly': ((Eval('state') != 'draft')
                | (If(~Eval('origin_id', 0), 0, Eval('origin_id', 0)) <= 0)),
            },
        depends=['state', 'origin_model', 'origin_id'])
    state = fields.Selection([
            ('draft', "Draft"),
            ('waiting', "Waiting"),
            ('approved', "Approved"),
            ('rejected', "Rejected"),
            ('done', "Done"),
            ('cancelled', "Cancelled"),
            ], "State", readonly=True, required=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('date', 'DESC'))
        cls._transitions |= set((
                ('draft', 'waiting'),
                ('waiting', 'draft'),
                ('waiting', 'approved'),
                ('waiting', 'rejected'),
                ('approved', 'done'),
                ('approved', 'draft'),
                ('draft', 'cancelled'),
                ('waiting', 'cancelled'),
                ('done', 'draft'),
                ('rejected', 'draft'),
                ('cancelled', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft', 'waiting']),
                    'depends': ['state'],
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(
                        ['waiting', 'done', 'cancelled']),
                    'icon': If(Eval('state').in_(['done', 'cancelled']),
                        'tryton-undo', 'tryton-back'),
                    'depends': ['state'],
                    },
                'wait': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'depends': ['state'],
                    },
                'approve': {
                    'invisible': ~Eval('state').in_(['waiting']),
                    'depends': ['state'],
                    },
                'reject': {
                    'invisible': ~Eval('state').in_(['waiting']),
                    'depends': ['state'],
                    },
                'process': {
                    'invisible': ~Eval('state').in_(['approved']),
                    'depends': ['state'],
                    },
                })

        origin_domain = []
        for model, domain in cls._origin_domains().items():
            origin_domain = If(Eval('origin_model') == model,
                domain, origin_domain)
        cls.origin.domain = [origin_domain]

        actions_domains = cls._actions_domains()
        actions_domain = [('action', 'in', actions_domains.pop(None))]
        for model, actions in actions_domains.items():
            actions_domain = If(Eval('origin_model') == model,
                [('action', 'in', actions)], actions_domain)
        cls.actions.domain = [actions_domain]

    @classmethod
    def _origin_domains(cls):
        return {
            'purchase.purchase': [
                If(Eval('supplier'),
                    ('party', '=', Eval('supplier')),
                    ()),
                ('company', '=', Eval('company')),
                ('state', 'in', ['confirmed', 'processing', 'done']),
                ],
            'purchase.line': [
                ('type', '=', 'line'),
                If(Eval('supplier'),
                    ('purchase.party', '=', Eval('supplier')),
                    ()),
                ('purchase.company', '=', Eval('company')),
                ('purchase.state', 'in', ['confirmed', 'processing', 'done']),
                ],
            'account.invoice': [
                If(Eval('supplier'),
                    ('party', '=', Eval('supplier')),
                    ()),
                ('company', '=', Eval('company')),
                ('type', '=', 'in'),
                ('state', 'in', ['posted', 'paid']),
                ],
            'account.invoice.line': [
                ('type', '=', 'line'),
                If(Eval('supplier'),
                    ('invoice.party', '=', Eval('supplier')),
                    ()),
                ('invoice.company', '=', Eval('company')),
                ('invoice.type', '=', 'in'),
                ('invoice.state', 'in', ['posted', 'paid']),
                ],
            }

    @classmethod
    def _actions_domains(cls):
        return {
            None: [],
            'purchase.purchase': ['purchase_return', 'scrap_goods'],
            'purchase.line': ['purchase_return', 'scrap_goods'],
            'account.invoice': ['credit_note'],
            'account.invoice.line': ['credit_note'],
            }

    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('type')
    def get_origin(self):
        if self.type:
            origin = self.type.origin
            return [('', ''), (origin.model, origin.name)]
        else:
            return []

    @fields.depends('origin', 'supplier')
    def on_change_origin(self):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        PurchaseLine = pool.get('purchase.line')
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')
        if not self.supplier and self.origin and self.origin.id >= 0:
            if isinstance(self.origin, Purchase):
                self.supplier = self.origin.party
            elif isinstance(self.origin, PurchaseLine):
                self.supplier = self.origin.purchase.party
            elif isinstance(self.origin, Invoice):
                self.supplier = self.origin.party
            elif isinstance(self.origin, InvoiceLine) and self.origin.invoice:
                self.supplier = self.origin.invoice.party

    @fields.depends('origin')
    def on_change_with_origin_id(self, name=None):
        if self.origin:
            return self.origin.id

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__class__.__name__

    @classmethod
    def view_attributes(cls):
        return [
            ('/tree', 'visual', If(Eval('state') == 'cancelled', 'muted', '')),
            ]

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Configuration = pool.get('purchase.configuration')

        config = Configuration(1)
        vlist = [v.copy() for v in vlist]
        default_company = cls.default_company()
        for values in vlist:
            if values.get('number') is None:
                values['number'] = config.get_multivalue(
                    'complaint_sequence',
                    company=values.get('company', default_company)).get()
        return super().create(vlist)

    @classmethod
    def copy(cls, complaints, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('number', None)
        return super().copy(complaints, default=default)

    @classmethod
    def delete(cls, complaints):
        for complaint in complaints:
            if complaint.state != 'draft':
                raise AccessError(
                    gettext('purchase_complaint.msg_complaint_delete_draft',
                        complaint=complaint.rec_name))
        super().delete(complaints)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, complaints):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, complaints):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def wait(cls, complaints):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approve(cls, complaints):
        pool = Pool()
        Configuration = pool.get('purchase.configuration')
        config = Configuration(1)
        with Transaction().set_context(
                queue_name='purchase',
                queue_scheduled_at=config.purchase_process_after):
            cls.__queue__.process(complaints)

    @classmethod
    @ModelView.button
    @Workflow.transition('rejected')
    def reject(cls, complaints):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def process(cls, complaints):
        pool = Pool()
        Action = pool.get('purchase.complaint.action')
        results = defaultdict(list)
        actions = defaultdict(list)
        for complaint in complaints:
            for action in complaint.actions:
                if action.result:
                    continue
                result = action.do()
                results[result.__class__].append(result)
                actions[result.__class__].append(action)
        for kls, records in results.items():
            kls.save(records)
            for action, record in zip(actions[kls], records):
                action.result = record
        Action.save(sum(list(actions.values()), []))


class Action(ModelSQL, ModelView):
    "Supplier Complaint Action"
    __name__ = 'purchase.complaint.action'

    _states = {
        'readonly': ((Eval('complaint_state') != 'draft')
            | Bool(Eval('result'))),
        }
    _depends = ['complaint_state', 'result']
    _line_states = {
        'invisible': ~Eval('_parent_complaint', {}
            ).get('origin_model', 'purchase.line').in_(
            ['purchase.line', 'account.invoice.line']),
        'readonly': _states['readonly'],
        }
    _line_depends = _depends

    complaint = fields.Many2One(
        'purchase.complaint', "Complaint", required=True, states=_states,
        depends=_depends)
    action = fields.Selection([
            ('purchase_return', "Create Purchase Return"),
            ('credit_note', "Create Credit Note"),
            ('scrap_goods', "Scrap Goods"),
            ], 'Action', states=_states)

    purchase_lines = fields.One2Many(
        'purchase.complaint.action-purchase.line', 'action', "Purchase Lines",
        states={
            'invisible': (Eval(
                    '_parent_complaint', {}
                    ).get('origin_model', 'purchase.purchase')
                != 'purchase.purchase'),
            'readonly': _states['readonly'],
            },
        depends=_depends,
        help='Leave empty for all lines.')

    invoice_lines = fields.One2Many(
        'purchase.complaint.action-account.invoice.line', 'action',
        "Invoice Lines",
        states={
            'invisible': Eval('_parent_complaint', {}
                ).get('origin_model', 'account.invoice.line'
                ) != 'account.invoice',
            'readonly': _states['readonly'],
            },
        depends=_depends,
        help='Leave empty for all lines.')

    quantity = fields.Float("Quantity",
        digits=(16, Eval('unit_digits', 2)),
        states=_line_states, depends=_line_depends + ['unit_digits'],
        help="Leave empty to use the quantity of the source line.")
    unit = fields.Function(
        fields.Many2One('product.uom', "Unit", states=_line_states,
            depends=_line_depends),
        'on_change_with_unit')
    unit_digits = fields.Function(
        fields.Integer("Unit Digits"), 'on_change_with_unit_digits')
    unit_price = fields.Numeric("Unit Price", digits=price_digits,
        states=_line_states, depends=_line_depends,
        help="Leave empty to use the price of the source line.")

    amount = fields.Function(fields.Numeric(
            "Amount", digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'on_change_with_amount')
    currency = fields.Function(fields.Many2One(
            'currency.currency', "Currency"),
        'on_change_with_currency')
    currency_digits = fields.Function(fields.Integer("Currency Digits"),
        'on_change_with_currency_digits')

    scrap_location = fields.Many2One('stock.location', "Scrap Location",
        states={
            'invisible': Eval('action') != 'scrap_goods',
            'required': Eval('action') == 'scrap_goods',
            'readonly': _states['readonly'],
            },
        domain=[
            ('waste_warehouses', '=', Eval('warehouse')),
            ],
        depends=_depends + ['action', 'warehouse'])
    warehouse = fields.Function(
        fields.Many2One('stock.location', "Warehouse"),
        'on_change_with_warehouse')

    result = fields.Reference("Result", selection='get_result', readonly=True)

    complaint_state = fields.Function(
        fields.Selection('get_complaint_states', "Complaint State"),
        'on_change_with_complaint_state')

    @fields.depends('complaint',
        '_parent_complaint.origin_model', '_parent_complaint.origin')
    def on_change_with_unit(self, name=None):
        if (self.complaint
                and self.complaint.origin_model in {
                    'purchase.line', 'account.invoice.line'}):
            return self.complaint.origin.unit.id

    @fields.depends('complaint',
        '_parent_complaint.origin_model', '_parent_complaint.origin')
    def on_change_with_unit_digits(self, name=None):
        if (self.complaint
                and self.complaint.origin_model in {
                    'purchase.line', 'account.invoice.line'}):
            return self.complaint.origin.unit.digits

    @fields.depends(
        'quantity', 'unit_price', 'currency', 'purchase_lines',
        'invoice_lines', 'complaint', '_parent_complaint.origin_model',
        '_parent_complaint.origin')
    def on_change_with_amount(self, name=None):
        if self.complaint:
            if self.complaint.origin_model in {
                    'purchase.line', 'account.invoice.line'}:
                if self.quantity is not None:
                    quantity = self.quantity
                else:
                    quantity = self.complaint.origin.quantity
                if self.unit_price is not None:
                    unit_price = self.unit_price
                else:
                    unit_price = self.complaint.origin.unit_price
                amount = Decimal(str(quantity)) * unit_price
                if self.currency:
                    amount = self.currency.round(amount)
                return amount
            elif self.complaint.origin_model == 'purchase.purchase':
                if not self.purchase_lines:
                    if self.complaint and self.complaint.origin:
                        return self.complaint.origin.untaxed_amount
                else:
                    return sum(
                        getattr(line, 'amount', None) or Decimal(0)
                        for line in self.purchase_lines)
            elif self.complaint.origin_model == 'account.invoice':
                if not self.invoice_lines:
                    if self.complaint and self.complaint.origin:
                        return self.complaint.origin.untaxed_amount
                else:
                    return sum(
                        getattr(line, 'amount', None) or Decimal(0)
                        for line in self.invoice_lines)

    @fields.depends(
        'complaint',
        '_parent_complaint.origin_model', '_parent_complaint.origin')
    def on_change_with_currency(self, name=None):
        if (self.complaint
                and self.complaint.origin_model in {
                    'purchase.purchase', 'purchase.line',
                    'account.invoice', 'account.invoice.line'}):
            return self.complaint.origin.currency.id

    @fields.depends(
        'complaint',
        '_parent_complaint.origin_model', '_parent_complaint.origin')
    def on_change_with_currency_digits(self, name=None):
        if (self.complaint
                and self.complaint.origin_model in {
                    'purchase.purchase', 'purchase.line',
                    'account.invoice', 'account.invoice.line'}):
            return self.complaint.origin.currency.digits

    @fields.depends(
        'complaint',
        '_parent_complaint.origin_model', '_parent_complaint.origin_id',
        '_parent_complaint.origin')
    def on_change_with_warehouse(self, name=None):
        if (self.complaint
                and self.complaint.origin_id
                and self.complaint.origin_id > 0):
            if self.complaint.origin_model == 'purchase.purchase':
                return self.complaint.origin.warehouse.id
            if self.complaint.origin_model == 'purchase.line':
                return self.complaint.origin.purchase.warehouse.id

    @classmethod
    def get_complaint_states(cls):
        pool = Pool()
        Complaint = pool.get('purchase.complaint')
        return Complaint.fields_get(['state'])['state']['selection']

    @fields.depends('complaint', '_parent_complaint.state')
    def on_change_with_complaint_state(self, name=None):
        if self.complaint:
            return self.complaint.state

    @classmethod
    def _get_result(cls):
        'Return list of Model names for result Reference'
        return [
            'purchase.purchase', 'account.invoice', 'stock.shipment.internal']

    @classmethod
    def get_result(cls):
        pool = Pool()
        Model = pool.get('ir.model')
        get_name = Model.get_name
        models = cls._get_result()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @classmethod
    def copy(cls, actions, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('result', None)
        return super().copy(actions, default=default)

    def do(self):
        return getattr(self, 'do_%s' % self.action)()

    def do_purchase_return(self):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        Line = pool.get('purchase.line')

        if isinstance(self.complaint.origin, (Purchase, Line)):
            default = {}
            if isinstance(self.complaint.origin, Purchase):
                purchase = self.complaint.origin
                if self.purchase_lines:
                    purchase_lines = [l.line for l in self.purchase_lines]
                    line2qty = {
                        l.line.id: l.get_quantity()
                        for l in self.purchase_lines}
                    line2price = {
                        l.line.id: l.get_unit_price()
                        for l in self.purchase_lines}
                    default['quantity'] = lambda o: line2qty.get(o['id'])
                    default['unit_price'] = lambda o: line2price.get(o['id'])
                else:
                    purchase_lines = [
                        l for l in purchase.lines if l.type == 'line']
            elif isinstance(self.complaint.origin, Line):
                purchase_line = self.complaint.origin
                purchase = purchase_line.purchase
                purchase_lines = [purchase_line]
                if self.quantity is not None:
                    default['quantity'] = self.quantity
                if self.unit_price is not None:
                    default['unit_price'] = self.unit_price
            return_purchase, = Purchase.copy([purchase], default={
                    'lines': None,
                    })
            default['purchase'] = return_purchase.id
            Line.copy(purchase_lines, default=default)
        else:
            return
        for line in return_purchase.lines:
            if line.type == 'line':
                line.quantity *= -1
        return_purchase.origin = self.complaint
        return_purchase.lines = return_purchase.lines  # Force saving
        return return_purchase

    def do_credit_note(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Line = pool.get('account.invoice.line')

        if isinstance(self.complaint.origin, (Invoice, Line)):
            line2qty = line2price = {}
            if isinstance(self.complaint.origin, Invoice):
                invoice = self.complaint.origin
                if self.invoice_lines:
                    invoice_lines = [l.line for l in self.invoice_lines]
                    line2qty = {l.line: l.quantity
                        for l in self.invoice_lines
                        if l.quantity is not None}
                    line2price = {l.line: l.unit_price
                        for l in self.invoice_lines
                        if l.unit_price is not None}
                else:
                    invoice_lines = [
                        l for l in invoice.lines if l.type == 'line']
            elif isinstance(self.complaint.origin, Line):
                invoice_line = self.complaint.origin
                invoice = invoice_line.invoice
                invoice_lines = [invoice_line]
                if self.quantity is not None:
                    line2qty = {invoice_line: self.quantity}
                if self.unit_price is not None:
                    line2price = {invoice_line: self.unit_price}
            with Transaction().set_context(_account_invoice_correction=True):
                credit_note, = Invoice.copy([invoice], default={
                        'lines': [],
                        })
                # Copy each line one by one to get negative and positive lines
                # following each other
                for invoice_line in invoice_lines:
                    qty = line2qty.get(invoice_line, invoice_line.quantity)
                    unit_price = invoice_line.unit_price - line2price.get(
                        invoice_line, invoice_line.unit_price)
                    Line.copy([invoice_line], default={
                            'invoice': credit_note.id,
                            'quantity': -qty,
                            'origin': str(self.complaint),
                            })
                    credit_line, = Line.copy([invoice_line], default={
                            'invoice': credit_note.id,
                            'quantity': qty,
                            'unit_price': unit_price,
                            'origin': str(self.complaint),
                            })
            credit_note.update_taxes([credit_note])
        else:
            return
        return credit_note

    def do_scrap_goods(self):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        Line = pool.get('purchase.line')
        Shipment = pool.get('stock.shipment.internal')

        if not isinstance(self.complaint.origin, (Purchase, Line)):
            return

        line2quantity = {}
        if isinstance(self.complaint.origin, Purchase):
            purchase = self.complaint.origin
            if self.purchase_lines:
                line2quantity = {
                    l.line: l.get_quantity() for l in self.purchase_lines}
            else:
                line2quantity = {
                    l: l.quantity for l in purchase.lines}
        elif isinstance(self.complaint.origin, Line):
            purchase = self.complaint.origin.purchase
            quantity = self.complaint.origin.quantity
            if self.quantity is not None:
                quantity = self.quantity
            line2quantity[self.complaint.origin] = quantity
        shipment = self.get_scrap_shipment(purchase, line2quantity)
        shipment.save()
        Shipment.wait([shipment])
        return shipment

    def get_scrap_shipment(self, purchase, line2quantity):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Move = pool.get('stock.move')

        shipment = Shipment()
        shipment.company = purchase.company
        shipment.from_location = purchase.warehouse.storage_location
        shipment.to_location = self.scrap_location
        moves = []
        for line, quantity in line2quantity.items():
            move = Move()
            move.from_location = shipment.from_location
            move.to_location = shipment.to_location
            move.product = line.product
            move.quantity = quantity
            move.uom = line.unit
            move.company = purchase.company
            moves.append(move)
        shipment.moves = moves
        return shipment

    @classmethod
    def delete(cls, actions):
        for action in actions:
            if action.result:
                raise AccessError(
                    gettext('purchase_complaint.msg_action_delete_result',
                        action=action.rec_name))
        super().delete(actions)


class _Action_Line:

    _states = {
        'readonly': (
            (Eval('complaint_state') != 'draft')
            | Bool(Eval('_parent_action.result', True))),
        }
    _depends = ['complaint_state']

    action = fields.Many2One('purchase.complaint.action', "Action",
        ondelete='CASCADE', select=True, required=True)
    quantity = fields.Float(
        "Quantity",
        digits=(16, Eval('unit_digits', 2)),
        states=_states,
        depends=_depends + ['unit_digits'],
        help="Leave empty to use the quantity of the source line.")
    unit = fields.Function(
        fields.Many2One('product.uom', "Unit"), 'on_change_with_unit')
    unit_digits = fields.Function(
        fields.Integer("Unit Digits"), 'on_change_with_unit_digits')
    unit_price = fields.Numeric(
        "Unit Price", digits=price_digits, states=_states, depends=_depends,
        help="Leave empty to use the price of the source line.")

    amount = fields.Function(fields.Numeric(
            "Amount", digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'on_change_with_amount')
    currency = fields.Function(fields.Many2One(
            'currency.currency', "Currency"),
        'on_change_with_currency')
    currency_digits = fields.Function(fields.Integer("Currency Digits"),
        'on_change_with_currency_digits')

    complaint_state = fields.Function(
        fields.Selection('get_complaint_states', "Complaint State"),
        'on_change_with_complaint_state')
    complaint_origin_id = fields.Function(
        fields.Integer("Complaint Origin ID"),
        'on_change_with_complaint_origin_id')

    def on_change_with_unit(self, name=None):
        raise NotImplementedError

    def on_change_with_unit_digits(self, name=None):
        raise NotImplementedError

    @fields.depends('currency', methods=['get_quantity', 'get_unit_price'])
    def on_change_with_amount(self, name=None):
        quantity = self.get_quantity() or 0
        unit_price = self.get_unit_price() or Decimal(0)
        amount = Decimal(str(quantity)) * unit_price
        if self.currency:
            amount = self.currency.round(amount)
        return amount

    def get_quantity(self):
        raise NotImplementedError

    def get_unit_price(self):
        raise NotImplementedError

    @fields.depends('action', '_parent_action.currency')
    def on_change_with_currency(self, name=None):
        if self.action and self.action.currency:
            return self.action.currency.id

    @fields.depends('action', '_parent_action.currency')
    def on_change_with_currency_digits(self, name=None):
        if self.action and self.action.currency:
            return self.action.currency.digits

    @classmethod
    def get_complaint_states(cls):
        pool = Pool()
        Complaint = pool.get('purchase.complaint')
        return Complaint.fields_get(['state'])['state']['selection']

    @fields.depends('action', '_parent_action.complaint',
        '_parent_action._parent_complaint.state')
    def on_change_with_complaint_state(self, name=None):
        if self.action and self.action.complaint:
            return self.action.complaint.state

    @fields.depends('action', '_parent_action.complaint',
        '_parent_action._parent_complaint.origin_id')
    def on_change_with_complaint_origin_id(self, name=None):
        if self.action and self.action.complaint:
            return self.action.complaint.origin_id


class Action_PurchaseLine(_Action_Line, ModelView, ModelSQL):
    "Supplier Complaint Action - Purchase Line"
    __name__ = 'purchase.complaint.action-purchase.line'

    line = fields.Many2One(
        'purchase.line', "Purchase Line",
        ondelete='RESTRICT', required=True,
        domain=[
            ('type', '=', 'line'),
            ('purchase', '=', Eval('complaint_origin_id', -1)),
            ],
        depends=['complaint_origin_id'])

    @fields.depends('line')
    def on_change_with_unit(self, name=None):
        if self.line:
            return self.line.unit.id

    @fields.depends('line')
    def on_change_with_unit_digits(self, name=None):
        if self.line:
            return self.line.unit.digits

    @fields.depends('quantity', 'line')
    def get_quantity(self):
        if self.quantity is not None:
            return self.quantity
        elif self.line:
            return self.line.quantity

    @fields.depends('unit_price', 'line')
    def get_unit_price(self):
        if self.unit_price is not None:
            return self.unit_price
        elif self.line:
            return self.line.unit_price


class Action_InvoiceLine(_Action_Line, ModelView, ModelSQL):
    "Supplier Complaint Action - Invoice Line"
    __name__ = 'purchase.complaint.action-account.invoice.line'

    line = fields.Many2One(
        'account.invoice.line', "Invoice Line",
        ondelete='RESTRICT', required=True,
        domain=[
            ('type', '=', 'line'),
            ('invoice', '=', Eval('complaint_origin_id', -1)),
            ],
        depends=['complaint_origin_id'])

    @fields.depends('line')
    def on_change_with_unit(self, name=None):
        if self.line:
            return self.line.unit.id

    @fields.depends('line')
    def on_change_with_unit_digits(self, name=None):
        if self.line:
            return self.line.unit.digits

    @fields.depends('quantity', 'line')
    def get_quantity(self):
        if self.quantity is not None:
            return self.quantity
        elif self.line:
            return self.line.quantity

    @fields.depends('unit_price', 'line')
    def get_unit_price(self):
        if self.unit_price is not None:
            return self.unit_price
        elif self.line:
            return self.line.unit_price
